import axios from 'axios';
import { API_ENDPOINT } from '../../config';

const postLogin = async (data) => {
  const response = await axios.post(`${API_ENDPOINT}/auth/login`, data).catch((error) => {
    throw error;
  });

  return response;
};

export default postLogin;
