import axios from 'axios';
import { API_ENDPOINT } from '../../config';
import { getToken } from '../user-data';

const getBook = async (id) => {
  const response = await axios
    .get(`${API_ENDPOINT}/books/${id}`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      },
    })
    .catch((error) => {
      throw error;
    });

  return response;
};

export default getBook;
