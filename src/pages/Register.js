import React from 'react';
import {
  StatusBar,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';
import { useMutation } from 'react-query';
import { useForm } from 'react-hook-form';
import { showMessage } from 'react-native-flash-message';
import { yupResolver } from '@hookform/resolvers/yup';

import { postRegister } from '../helpers/api';
import { registerValidation } from '../helpers/form-validation';
import { Input } from '../components';
import styles from '../styles/pages/Register';

function Register({ navigation }) {
  const insets = useSafeAreaInsets();

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      email: '',
      password: '',
    },
    resolver: yupResolver(registerValidation),
  });

  const { mutate: registerMutation, isLoading } = useMutation(
    async (registerData) => {
      const response = await postRegister(registerData);
      return response.data;
    },
    {
      throwOnError: true,
      onSuccess: (data) => {
        showMessage({
          message: data.message,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$success[500]'),
          titleStyle: { textAlign: 'center' },
          floating: true,
          statusBarHeight: insets.top + 6,
        });
        navigation.replace('Verification');
      },
      onError: (error) => {
        showMessage({
          message: `[${error.name.toUpperCase()}] ${error.response.data.message}`,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: { textAlign: 'center' },
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    }
  );

  const onSubmit = async (data) => {
    registerMutation(data);
  };

  return (
    <>
      <StatusBar translucent backgroundColor="transparent" barStyle="light-content" />
      <SafeAreaView style={styles.header} edges={['right', 'top', 'left']}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <View style={styles.header.action.button}>
            <Icon name="arrow-back-outline" style={styles.header.action.icon} />
          </View>
        </TouchableOpacity>
      </SafeAreaView>
      <SafeAreaView style={styles.safeArea} edges={['right', 'bottom', 'left']}>
        <ScrollView contentContainerStyle={styles.container}>
          <View style={styles.form}>
            <Text style={styles.form.header}>Register</Text>
            <Text style={styles.form.description}>
              Get the best experience reading book with join us!
            </Text>
            <Input
              name="name"
              placeholder="Full Name"
              icon="user"
              control={control}
              errors={errors}
              disable={isLoading}
            />
            <Input
              name="email"
              placeholder="Email"
              icon="at-sign"
              control={control}
              errors={errors}
              disable={isLoading}
            />
            <Input
              name="password"
              placeholder="Password"
              icon="lock"
              control={control}
              errors={errors}
              disable={isLoading}
              secureTextEntry
            />
            <TouchableOpacity
              style={[
                styles.form.button,
                !isLoading ? styles.form.button.active : styles.form.button.disabled,
              ]}
              onPress={handleSubmit(onSubmit)}
              disabled={isLoading}
            >
              {isLoading && (
                <ActivityIndicator
                  size="small"
                  style={styles.form.button.indicator}
                  color={styles.form.button.indicator.color}
                />
              )}
              <Text style={styles.form.button.text}>Register</Text>
            </TouchableOpacity>
            <View style={styles.form.navigation}>
              <Text style={styles.form.navigation.text}>Already have an account?</Text>
              <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                <Text style={styles.form.navigation.link}>Login</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

Register.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
    navigate: PropTypes.func.isRequired,
    replace: PropTypes.func.isRequired,
  }).isRequired,
};

export default Register;
