import React, { useState } from 'react';
import { StatusBar, View, Text, ScrollView, RefreshControl, TouchableOpacity } from 'react-native';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import EStyleSheet from 'react-native-extended-stylesheet';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Ionicons';
import { showMessage } from 'react-native-flash-message';
import { useDispatch } from 'react-redux';
import { useQuery } from 'react-query';

import { getBookList } from '../helpers/api';
import { saveLoginData } from '../redux/action';
import { getName } from '../helpers/user-data';
import { Book } from '../components';
import styles from '../styles/pages/HomeScreen';

function HomeScreen({ navigation }) {
  const [recommendedBook, setRecommendedBook] = useState(null);
  const [popularBook, setPopularBook] = useState(null);
  const insets = useSafeAreaInsets();
  const dispatch = useDispatch();
  const today = new Date();
  const currentHour = today.getHours();

  const getGreeting = () => {
    if (currentHour < 12) {
      return 'Good Morning';
    }
    if (currentHour < 18) {
      return 'Good Afternoon';
    }
    return 'Good Evening';
  };

  const { isFetching, isError, refetch } = useQuery(
    'movies',
    async () => {
      try {
        const response = await getBookList();
        return response.data;
      } catch (error) {
        throw new Error(error.message);
      }
    },
    {
      onSuccess: (data) => {
        const recommended = data.results
          .sort((min, max) => {
            return max.average_rating - min.average_rating;
          })
          .slice(0, 6);
        setRecommendedBook(recommended);
        const popular = data.results.sort((min, max) => {
          return min.id - max.id;
        });
        setPopularBook(popular);
      },
      onError: (error) => {
        showMessage({
          message: `[${error.name.toUpperCase()}] ${error.message}`,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: { textAlign: 'center' },
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    }
  );

  return (
    <>
      <StatusBar translucent backgroundColor="transparent" barStyle="light-content" />
      <SafeAreaView style={styles.header} edges={['right', 'top', 'left']}>
        <Text style={styles.header.greeting}>
          {getGreeting()}, {'\n'}
          <Text style={styles.header.name}>{getName()}</Text>
        </Text>
        <TouchableOpacity
          onPress={() => {
            dispatch(saveLoginData(null));
            navigation.replace('Login');
          }}
        >
          <Icon name="power" style={styles.header.icon} />
        </TouchableOpacity>
      </SafeAreaView>
      <SafeAreaView style={styles.safeArea} edges={['right', 'bottom', 'left']}>
        <ScrollView
          contentContainerStyle={styles.container}
          refreshControl={<RefreshControl refreshing={isFetching} onRefresh={refetch} />}
        >
          <View style={styles.recomendation}>
            <Text style={styles.recomendation.header}>Recommended</Text>
            <ScrollView
              horizontal
              contentContainerStyle={styles.recomendation.content}
              showsHorizontalScrollIndicator={false}
            >
              {isFetching || isError
                ? [1, 2, 3, 4, 5, 6].map((data) => <Book key={data} type="recommended" />)
                : recommendedBook.map((data) => (
                    <Book
                      key={Math.random()}
                      data={data}
                      type="recommended"
                      navigation={navigation}
                    />
                  ))}
            </ScrollView>
          </View>
          <View style={styles.popular}>
            <Text style={styles.popular.header}>Popular Book</Text>
            <View style={styles.popular.content}>
              {isFetching || isError
                ? [1, 2, 3, 4, 5, 6].map((data) => <Book key={data} type="popular" />)
                : popularBook.map((data) => (
                    <Book key={Math.random()} data={data} type="popular" navigation={navigation} />
                  ))}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

HomeScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    replace: PropTypes.func.isRequired,
  }).isRequired,
};

export default HomeScreen;
