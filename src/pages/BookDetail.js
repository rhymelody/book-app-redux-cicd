import React, { useState } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Share,
  RefreshControl,
  StatusBar,
  SafeAreaView as BaseSafeAreaView,
} from 'react-native';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import EStyleSheet from 'react-native-extended-stylesheet';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import Icon from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';
import { showMessage } from 'react-native-flash-message';
import { useQuery } from 'react-query';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import notifee, { AndroidImportance } from '@notifee/react-native';

import { getBook } from '../helpers/api';
import styles from '../styles/pages/BookDetail';
import { Currency } from '../components';

function BookDetail({ route, navigation }) {
  const [bookData, setBookData] = useState(null);
  const insets = useSafeAreaInsets();

  const { isFetching, isError, refetch } = useQuery(
    'book-detail',
    async () => {
      try {
        const response = await getBook(route.params.id);
        return response.data;
      } catch (error) {
        throw new Error(error.message);
      }
    },
    {
      onSuccess: (response) => {
        setBookData(response);
      },
      onError: (error) => {
        showMessage({
          message: `[${error.name.toUpperCase()}] ${error.message}`,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: { textAlign: 'center' },
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    }
  );

  async function onDisplayNotification() {
    const channelId = await notifee.createChannel({
      id: 'default',
      name: 'Bukuku',
      importance: AndroidImportance.HIGH,
    });
    await notifee.requestPermission();
    await notifee.displayNotification({
      title: 'Bukuku',
      body: `You just like ${isFetching || isError ? 'this' : `${bookData.title}'s`} book!`,
      android: {
        channelId,
      },
    });
  }

  return (
    <>
      <StatusBar translucent backgroundColor="transparent" barStyle="light-content" />
      <BaseSafeAreaView style={styles.notification} />
      <SafeAreaView style={styles.safeArea} edges={['right', 'bottom', 'left']}>
        <ParallaxScrollView
          style={{ overflow: 'hidden' }}
          refreshControl={<RefreshControl refreshing={isFetching} onRefresh={refetch} />}
          stickyHeaderHeight={styles.header.minHeight}
          parallaxHeaderHeight={styles.header.maxHeight}
          backgroundColor={styles.header.sticky.backgroundColor}
          renderBackground={() => (
            <View style={styles.header}>
              {isFetching || isError ? (
                <ShimmerPlaceHolder
                  LinearGradient={LinearGradient}
                  style={styles.header.tint.image}
                />
              ) : (
                <View style={styles.header.tint}>
                  <Image
                    key="background"
                    source={{
                      uri: bookData.cover_image,
                    }}
                    style={[styles.header.tint.image, { opacity: 0.25 }]}
                    blurRadius={7.5}
                  />
                </View>
              )}
            </View>
          )}
          renderForeground={() => (
            <View key="parallax-header" style={styles.header.book}>
              <View style={styles.header.book.content}>
                <View style={styles.header.book.content.contentImage}>
                  {isFetching || isError ? (
                    <ShimmerPlaceHolder
                      LinearGradient={LinearGradient}
                      style={styles.header.book.content.contentImage.image}
                    />
                  ) : (
                    <Image
                      style={styles.header.book.content.contentImage.image}
                      resizeMode="cover"
                      source={{
                        uri: bookData.cover_image,
                      }}
                    />
                  )}
                </View>
                <View style={styles.header.book.content.contentDetails}>
                  <Text
                    style={styles.header.book.content.contentDetails.title}
                    adjustsFontSizeToFit
                    numberOfLines={2}
                  >
                    {isFetching || isError ? (
                      <ShimmerPlaceHolder
                        LinearGradient={LinearGradient}
                        shimmerStyle={styles.shimmer.title}
                        shimmerColors={styles.shimmer.title.color}
                      />
                    ) : (
                      bookData.title
                    )}
                  </Text>
                  <Text style={styles.header.book.content.contentDetails.author}>
                    {isFetching || isError ? (
                      <ShimmerPlaceHolder
                        LinearGradient={LinearGradient}
                        shimmerStyle={styles.shimmer.author}
                      />
                    ) : (
                      bookData.author
                    )}
                  </Text>
                  <Text style={styles.header.book.content.contentDetails.publisher}>
                    {isFetching || isError ? (
                      <ShimmerPlaceHolder
                        LinearGradient={LinearGradient}
                        shimmerStyle={styles.shimmer.publisher}
                      />
                    ) : (
                      bookData.publisher
                    )}
                  </Text>
                </View>
              </View>
            </View>
          )}
          renderStickyHeader={() => (
            <View key="sticky-header" style={[styles.header.sticky, { backgroundColor: 'pink' }]} />
          )}
          renderFixedHeader={() => (
            <View key="fixed-header" style={styles.header.navigation}>
              <View style={styles.header.navigation.actionLeft}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                  <View style={styles.header.navigation.action.button}>
                    <Icon name="arrow-back-outline" style={styles.header.navigation.action.icon} />
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.header.navigation.actionRight}>
                <TouchableOpacity
                  style={{ marginRight: 5 }}
                  onPress={() => onDisplayNotification()}
                >
                  <View style={styles.header.navigation.action.button}>
                    <Icon name="heart-outline" style={styles.header.navigation.action.icon} />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ marginLeft: 5 }}
                  onPress={() =>
                    Share.share(
                      {
                        title: 'Bukuku',
                        message: `Read ${
                          isFetching || isError ? 'Book' : bookData.title
                        } in best Experience only on Bukuku!`,
                      },
                      { dialogTitle: 'Bukuku' }
                    )
                  }
                >
                  <View style={styles.header.navigation.action.button}>
                    <Icon name="arrow-redo-outline" style={styles.header.navigation.action.icon} />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          )}
        >
          <View style={styles.container}>
            <View style={styles.content.action}>
              <View style={styles.content.action.rating}>
                <Text style={styles.content.action.rating.header}>Rating</Text>
                <View style={styles.content.action.rating.content}>
                  {isFetching || isError ? (
                    <ShimmerPlaceHolder
                      LinearGradient={LinearGradient}
                      shimmerStyle={styles.shimmer.rating}
                      shimmerColors={styles.shimmer.rating.color}
                    />
                  ) : (
                    <>
                      <Icon name="star-sharp" style={styles.content.action.rating.content.icon} />
                      <Text style={styles.content.action.rating.content.text}>
                        {bookData.average_rating}
                      </Text>
                    </>
                  )}
                </View>
              </View>
              <View style={styles.content.action.sale}>
                <Text style={styles.content.action.sale.header}>Total Sale</Text>
                {isFetching || isError ? (
                  <ShimmerPlaceHolder
                    LinearGradient={LinearGradient}
                    shimmerStyle={styles.shimmer.sale}
                  />
                ) : (
                  <Text style={styles.content.action.sale.content}>{bookData.total_sale}</Text>
                )}
              </View>
              <View style={styles.content.action.buy}>
                {isFetching || isError ? (
                  <ShimmerPlaceHolder
                    LinearGradient={LinearGradient}
                    shimmerStyle={styles.shimmer.button}
                    shimmerColors={styles.shimmer.button.color}
                  />
                ) : (
                  <TouchableOpacity style={styles.content.action.buy.button}>
                    <Text style={styles.content.action.buy.button.text}>
                      Buy <Currency value={bookData.price} />
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </View>
            <View style={styles.content.overview}>
              <Text style={styles.content.overview.header}>Overview</Text>
              {isFetching || isError ? (
                ['75%', '90%', '87.5%', '95%', '65%'].map((value) => (
                  <ShimmerPlaceHolder
                    LinearGradient={LinearGradient}
                    shimmerStyle={[styles.shimmer.synopsis, { width: value }]}
                    key={value}
                  />
                ))
              ) : (
                <Text style={styles.content.overview.content}>{bookData.synopsis}</Text>
              )}
            </View>
          </View>
        </ParallaxScrollView>
      </SafeAreaView>
    </>
  );
}

BookDetail.propTypes = {
  route: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};

export default BookDetail;
