import React from 'react';
import PropTypes from 'prop-types';
import { StatusBar, ScrollView, View, Text, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Feather';

import styles from '../styles/pages/Verification';

function Verification({ navigation }) {
  return (
    <>
      <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" />
      <SafeAreaView style={styles.safeArea}>
        <ScrollView contentContainerStyle={styles.container}>
          <View style={styles.header}>
            <Text style={styles.header.text}>Registration Completed!</Text>
          </View>
          <View style={styles.content}>
            <View style={styles.content.iconContainer}>
              <Icon name="check" style={styles.content.iconContainer.icon} />
            </View>
            <Text style={styles.content.text}>
              We&apos;ve sent email verification to your email.
            </Text>
          </View>
          <View style={styles.footer}>
            <TouchableOpacity
              style={styles.footer.button}
              onPress={() => navigation.navigate('Login')}
            >
              <Text style={styles.footer.button.text}>Back to login</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

Verification.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default Verification;
