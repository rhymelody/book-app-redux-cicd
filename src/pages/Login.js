import React from 'react';
import PropTypes from 'prop-types';
import {
  StatusBar,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { SafeAreaView, useSafeAreaInsets } from 'react-native-safe-area-context';
import { useMutation } from 'react-query';
import { useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { showMessage } from 'react-native-flash-message';
import { yupResolver } from '@hookform/resolvers/yup';

import { postLogin } from '../helpers/api';
import { saveLoginData } from '../redux/action';
import { loginValidation } from '../helpers/form-validation';
import { Input } from '../components';
import bannerIMG from '../assets/images/banner.png';
import styles from '../styles/pages/Login';

function Login({ navigation }) {
  const insets = useSafeAreaInsets();
  const dispatch = useDispatch();

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      email: '',
      password: '',
    },
    resolver: yupResolver(loginValidation),
  });

  const { mutate: loginMutation, isLoading } = useMutation(
    async (loginData) => {
      const response = await postLogin(loginData);
      return response.data;
    },
    {
      throwOnError: true,
      onSuccess: (data) => {
        if (data.user.isEmailVerified) {
          dispatch(saveLoginData(data));
          navigation.replace('Home');
        } else {
          navigation.replace('Verification');
        }
      },
      onError: (error) => {
        showMessage({
          message: `[${error.name.toUpperCase()}] ${error.response.data.message}`,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: { textAlign: 'center' },
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    }
  );

  const onSubmit = async (data) => {
    loginMutation(data);
  };

  return (
    <>
      <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" />
      <SafeAreaView style={styles.safeArea}>
        <ScrollView contentContainerStyle={styles.container}>
          <Image source={bannerIMG} resizeMode="contain" style={styles.banner} />
          <View style={styles.form}>
            <Input
              name="email"
              placeholder="Email"
              icon="at-sign"
              control={control}
              errors={errors}
              disable={isLoading}
            />
            <Input
              name="password"
              placeholder="Password"
              icon="lock"
              control={control}
              errors={errors}
              disable={isLoading}
              secureTextEntry
            />
            <TouchableOpacity
              style={[
                styles.form.button,
                !isLoading ? styles.form.button.active : styles.form.button.disabled,
              ]}
              onPress={handleSubmit(onSubmit)}
              disabled={isLoading}
            >
              {isLoading && (
                <ActivityIndicator
                  size="small"
                  style={styles.form.button.indicator}
                  color={styles.form.button.indicator.color}
                />
              )}
              <Text style={styles.form.button.text}>Login</Text>
            </TouchableOpacity>
            <View style={styles.form.navigation}>
              <Text style={styles.form.navigation.text}>Don&apos;t have an account?</Text>
              <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                <Text style={styles.form.navigation.link}>Register</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

Login.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    replace: PropTypes.func.isRequired,
  }).isRequired,
};

export default Login;
