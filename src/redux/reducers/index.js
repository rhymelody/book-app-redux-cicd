import { FETCH_LOGIN } from '../types';

const initialState = {
  data: null,
};

const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_LOGIN:
      return {
        ...state,
        data: action.payload,
      };
    default:
      return state;
  }
};

export default loginReducer;
