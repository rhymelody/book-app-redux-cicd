import { FETCH_LOGIN } from '../types';

export const saveLoginData = (data) => ({
  type: FETCH_LOGIN,
  payload: data,
});
