import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from '@redux-devtools/extension';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import loginReducer from '../reducers';

export const store = createStore(loginReducer, composeWithDevTools(applyMiddleware(thunk, logger)));
