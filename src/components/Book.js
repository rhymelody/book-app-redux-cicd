import React from 'react';
import { TouchableOpacity, Image, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import Icon from 'react-native-vector-icons/Ionicons';

import styles from '../styles/components/Book';
import Currency from './Currency';

function Book({ data, type, navigation }) {
  return (
    <View style={styles.container[type]}>
      <TouchableOpacity
        style={styles.contentImage}
        onPress={() => data && navigation.navigate('Book Detail', { id: data.id })}
      >
        {data ? (
          <>
            {type !== 'recommended' && (
              <View style={styles.contentImage.rating}>
                <Icon name="star-sharp" style={styles.contentImage.rating.icon} />
                <Text style={styles.contentImage.rating.text}>{data.average_rating}</Text>
              </View>
            )}

            <Image
              source={{
                uri: data.cover_image,
              }}
              style={styles.contentImage.image[type]}
              resizeMode="cover"
            />
          </>
        ) : (
          <ShimmerPlaceHolder
            LinearGradient={LinearGradient}
            shimmerStyle={styles.contentImage.image[type]}
          />
        )}
      </TouchableOpacity>
      <View style={styles.contentDetails}>
        {data ? (
          <Text style={styles.contentDetails.title} numberOfLines={1}>
            {data.title}
          </Text>
        ) : (
          <ShimmerPlaceHolder
            LinearGradient={LinearGradient}
            shimmerStyle={styles.shimmer.title}
            shimmerColors={styles.shimmer.title.color}
          />
        )}
        {data ? (
          <Text style={styles.contentDetails.subtitle} numberOfLines={1}>
            {`${data.author} - ${data.publisher}`}
          </Text>
        ) : (
          <ShimmerPlaceHolder
            LinearGradient={LinearGradient}
            shimmerStyle={styles.shimmer.subtitle}
          />
        )}
        <View style={styles.contentDetails.row}>
          {data ? (
            <Text style={styles.contentDetails.price}>
              <Currency value={data.price} />
            </Text>
          ) : (
            <ShimmerPlaceHolder
              LinearGradient={LinearGradient}
              shimmerStyle={styles.shimmer.price[type]}
            />
          )}
          {type === 'recommended' &&
            (data ? (
              <View style={styles.contentDetails.rating}>
                <Icon name="star-sharp" style={styles.contentDetails.rating.icon} />
                <Text style={styles.contentDetails.rating.text}>{data.average_rating}</Text>
              </View>
            ) : (
              <ShimmerPlaceHolder
                LinearGradient={LinearGradient}
                shimmerStyle={styles.shimmer.rating}
                shimmerColors={styles.shimmer.rating.color}
              />
            ))}
        </View>
      </View>
    </View>
  );
}

Book.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string,
    cover_image: PropTypes.string,
    title: PropTypes.string,
    author: PropTypes.string,
    publisher: PropTypes.string,
    price: PropTypes.string,
    average_rating: PropTypes.string,
  }),
  type: PropTypes.string.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }),
};

Book.defaultProps = {
  data: null,
  navigation: null,
};

export default Book;
