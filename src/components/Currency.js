import React from 'react';
import NumberFormat from 'react-number-format';
import PropTypes from 'prop-types';

function Currency({ value }) {
  return (
    <NumberFormat
      prefix="Rp "
      thousandSeparator="."
      decimalSeparator=","
      value={value}
      displayType="text"
      renderText={(render) => render}
    />
  );
}

Currency.propTypes = {
  value: PropTypes.string,
};

Currency.defaultProps = {
  value: '0',
};

export default Currency;
