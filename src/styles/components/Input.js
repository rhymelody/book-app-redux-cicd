import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    marginBottom: '1.25rem',
  },
  form: {
    flexDirection: 'row',
    alignItems: 'center',
    position: 'relative',
  },
  icon: {
    top: '.125rem',
    color: '$secondary[500]',
    fontSize: '1.25rem',
    marginRight: '.75rem',
  },
  input: {
    fontSize: '.925rem',
    borderColor: '$secondary[500]',
    backgroundColor: '$base.white',
    borderBottomWidth: 1,
    paddingRight: '1rem',
    paddingVertical: '.5rem',
    marginRight: '.625rem',
    flex: 1,
  },
  active: {
    color: '$netral[500]',
  },
  disabled: {
    color: '$secondary[500]',
  },
  placeholder: { color: '$secondary[500]' },
  error: {
    marginTop: '.425rem',
    marginBottom: '-.725rem',
    color: '$danger[500]',
  },
});

export default styles;
