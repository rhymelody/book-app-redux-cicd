import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  safeArea: {
    backgroundColor: '$background',
    flexGrow: 1,
    paddingBottom: '7.375rem',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '$primary[500]',
    padding: '1.5rem',
    action: {
      button: {
        backgroundColor: 'rgba(250, 250, 250, 0.675)',
        height: '2.25rem',
        width: '2.25rem',
        borderRadius: '2.25rem / 2',
        alignItems: 'center',
        justifyContent: 'center',
      },
      icon: {
        color: '$netral[700]',
        fontSize: '1.275rem',
      },
    },
  },
  container: {
    backgroundColor: '$background',
    padding: '1.5rem',
    width: '100%',
    flexGrow: 1,
  },
  form: {
    header: {
      marginTop: '.675rem',
      color: '$netral[900]',
      fontSize: '1.325rem',
      marginBottom: '.5rem',
      fontWeight: '700',
      textAlign: 'center',
    },
    description: {
      color: '$secondary[500]',
      fontSize: '.85rem',
      marginBottom: '2.5rem',
      textAlign: 'center',
    },
    button: {
      marginTop: '1.25rem',
      marginBottom: '1.25rem',
      paddingVertical: '.75rem',
      paddingHorizontal: '1rem',
      borderRadius: '.5rem',
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '$primary[500]',
      active: {
        opacity: 1,
      },
      disabled: {
        opacity: 0.2,
      },
      indicator: {
        color: '$base.white',
        marginRight: '1rem',
      },
      text: {
        color: '$base.white',
        fontSize: '1rem',
        fontWeight: '600',
      },
    },
    navigation: {
      alignItems: 'center',
      text: {
        fontSize: '.875rem',
        fontWeight: '500',
        color: '$secondary[500]',
        marginBottom: '.25rem',
      },
      link: {
        fontSize: '.875rem',
        fontWeight: '800',
        color: '$netral[500]',
      },
    },
  },
});

export default styles;
