import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  safeArea: {
    backgroundColor: '$background',
    flexGrow: 1,
    paddingBottom: '6.375rem',
  },
  header: {
    flexGrow: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '$primary[500]',
    padding: '1.5rem',
    greeting: {
      color: '$base.white',
      fontSize: '1rem',
      fontWeight: '400',
    },
    name: {
      color: '$base.white',
      fontSize: '1rem',
      fontWeight: '500',
    },
    icon: {
      fontSize: '1.5rem',
      color: '$base.white',
    },
  },
  container: {
    backgroundColor: '$background',
    padding: '1.5rem',
    width: '100%',
    flexGrow: 1,
  },
  recomendation: {
    marginBottom: '2rem',
    header: {
      color: '$netral[500]',
      fontSize: '1.125rem',
      fontWeight: 'bold',
      marginBottom: '1rem',
    },
    content: {
      flexDirection: 'row',
      marginHorizontal: '-.5rem',
    },
  },
  popular: {
    header: {
      color: '$netral[500]',
      fontSize: '1.125rem',
      fontWeight: 'bold',
      marginBottom: '1rem',
    },
    content: {
      flexDirection: 'row',
      width: '100%',
      flexWrap: 'wrap',
      margin: '-.5rem',
    },
  },
});

export default styles;
